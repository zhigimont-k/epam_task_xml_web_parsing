package by.epam.task4.command;

public enum TypeCommand {
    LOGIN, LOGOUT, REGISTER, UPLOAD, CHANGE_LOCALE
}
